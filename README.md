# APIハンズオン

## 本リポジトリについて
モバイルハンズオンの題材の完成したアプリです。

`api-handson.pdf` にハンズオンのテキストも配置しております。

## 事前準備

https://tidev-aizu.connpass.com/event/203316/ の「事前準備」をごらんください。

## 起動方法

本アプリは、DBからデータを取得します。  
そのため、アプリの起動前にDBを起動します。
### DBの起動
REAT APIアプリが接続するDBサーバを起動します。

`todo-app-final/backend/` ディレクトリで以下のコマンド実行します。
```
docker-compose -f docker/docker-compose.dev.yml up -d
```

次のコマンドで起動を確認します。
```
docker-compose -f docker/docker-compose.dev.yml ps
```

以下の実行例のように `State` が `UP` となっていればDBが起動しています。
```
$ docker-compose -f docker/docker-compose.dev.yml ps
      Name                     Command              State           Ports
----------------------------------------------------------------------------------
docker_postgres_1   docker-entrypoint.sh postgres   Up      0.0.0.0:5432->5432/tcp
```

### REST APIのアプリの本体の起動

`todo-app-final/backend/` ディレクトリで以下のコマンド実行します。
```
mvn jetty:run
```

ブラウザを起動し次のURLにアクセスします。
http://localhost:9080/api/test

画面に `{“status”:“ok”}` と表示されれば成功です。