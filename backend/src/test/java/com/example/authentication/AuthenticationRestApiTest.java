package com.example.authentication;

import com.example.openapi.OpenApiValidator;
import nablarch.fw.web.HttpResponse;
import nablarch.fw.web.RestMockHttpRequest;
import nablarch.test.core.http.SimpleRestTestSupport;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.nio.file.Paths;
import java.util.Map;

public class AuthenticationRestApiTest extends SimpleRestTestSupport {

    private final static OpenApiValidator openApiValidator
            = new OpenApiValidator(Paths.get("rest-api-specification/openapi.yaml"));

    @Test
    public void RESTAPIでログインできる() throws Exception {
        RestMockHttpRequest request = post("/api/login")
                .setHeader("Content-Type", MediaType.APPLICATION_JSON)
                .setBody(Map.of(
                        "userName", "login-test",
                        "password", "pass"));
        HttpResponse response = sendRequest(request);

        assertStatusCode("ログイン", HttpResponse.Status.NO_CONTENT, response);

        openApiValidator.validate("login", request, response);
    }

    @Test
    public void パスワードが不一致の場合_ログインに失敗して401になる() throws Exception {
        RestMockHttpRequest request = post("/api/login")
                .setHeader("Content-Type", MediaType.APPLICATION_JSON)
                .setBody(Map.of(
                        "userName", "login-test",
                        "password", "fail"));
        HttpResponse response = sendRequest(request);

        assertStatusCode("ログイン", HttpResponse.Status.UNAUTHORIZED, response);

        openApiValidator.validate("login", request, response);
    }

    @Test
    public void 名前が不一致の場合_ログインに失敗して401になる() throws Exception {
        RestMockHttpRequest request = post("/api/login")
                .setHeader("Content-Type", MediaType.APPLICATION_JSON)
                .setBody(Map.of(
                        "userName", "fail-test",
                        "password", "pass"));
        HttpResponse response = sendRequest(request);

        assertStatusCode("ログイン", HttpResponse.Status.UNAUTHORIZED, response);

        openApiValidator.validate("login", request, response);
    }

    @Test
    public void RESTAPIでログアウトできる() throws Exception {
        RestMockHttpRequest request = post("/api/logout");
        HttpResponse response = sendRequest(request);

        assertStatusCode("ログアウト", HttpResponse.Status.NO_CONTENT, response);

        openApiValidator.validate("logout", request, response);
    }
}
