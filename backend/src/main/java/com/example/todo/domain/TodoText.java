package com.example.todo.domain;

public class TodoText {

    private final String text;

    public TodoText(String text) {
        this.text = text;
    }

    public String value() {
        return text;
    }
}
